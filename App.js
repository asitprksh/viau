//exp start --tunnel
import React, { Component } from "react";
import { StyleSheet, StatusBar } from "react-native";
import { Container, Content, Picker, Button, Text, View } from "native-base";
import Expo from "expo";
import  Welcome  from "./Screens/Welcome/index";

export default class viau extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf")
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return <Welcome />;
    <View style={styles.container}>
    <StatusBar
   backgroundColor="rgba(0, 0, 0, 0.5)"
   barStyle="light-content"
   />    

    </View>
  }
}

const styles = StyleSheet.create({  
  container : {
    backgroundColor : 'rgba(0, 0, 0, 0.5)',
    flex:1,
    alignItems:'center',   
    justifyContent:'center'
    
  }

});