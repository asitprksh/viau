import React, { Component } from 'react';
import { StyleSheet, 
         Text,
         View,
         Image,
        ImageBackground } from 'react-native';
import { Container,
         Header,
         Content,
         Button,} from 'native-base';
import { createStackNavigator } from 'react-navigation';
import * as Animatable from 'react-native-animatable';
import Login from "../Login/LoginPage";  
import Signup  from "../Signup/SignupPage"; 
      
     
 export default class WelcomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };     

  render() {
    return (     
      <View style={{flex : 1}}>
        <ImageBackground
          source={require('./asset/backgroundwelcome.png')}      
          style={{flex: 1}}    
        >                              
         <Animatable.View 
                animation="zoomIn" iterationCount={1}     
                style={{
                justifyContent: 'center',
                alignItems: 'center',               
                flex: 1
                     }}>
             <Image style = {styles.logo} source={require('./asset/android-icon-96x96.png')}
             />
          </Animatable.View>      
          
        </ImageBackground>
        <Animatable.View 
                        animation="slideInUp" iterationCount={1}
                         style={{height:150,
                                 backgroundColor: 'white',
                               }}>
        
          <Button 
                  block
                 dark
                
                style={[styles.button, {width: 300},{marginTop: 20},{alignSelf:"center"}]}
        onPress = {() => this.props.navigation.navigate('Login')}
        title="Check Text Input Is Empty or Not"
                color="#2196F3"
                >
              <Text style={{
                            fontWeight:'bold' ,fontSize:15,color:"#FFFFFF"
                          }}
                        
              >LOGIN</Text>  
        </Button>
       
        
        <Button block dark style={[styles.button, {width: 300},{marginTop: 15},{alignSelf:"center"}]}
        onPress = {() => this.props.navigation.navigate('Signup')}>
              <Text style={{
                            fontWeight:'bold' ,fontSize:15,color:"#FFFFFF"
                          }}
                   
              >SIGNUP</Text>  
        </Button>
        
        </Animatable.View>
        
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



