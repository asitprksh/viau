import React, { Component } from "react";
import LoginScreen from "../Login/index";
import signupScreen from "../Signup/index";
import { createStackNavigator } from "react-navigation";
import WelcomeScreen from "./Welcome"

const Welcome = createStackNavigator(
  {
    Welcome: { screen: WelcomeScreen },
    Login : { screen: LoginScreen },
    Signup : { screen: signupScreen}
  }
);
 
export default Welcome;

