import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { MapView } from "expo";


export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <MapView   
        style={{
          flex: 1
        }}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
      />
    );
  }
}



/*import React, { Component } from "react";
import { StyleSheet,
         Text,
         View,
        ImageBackground } from 'react-native';


 export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };     
  render() {
    return (
      <View style={styles.container}>
      
        <Text>Login page</Text>
      </View>
    );
  }    
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#bdbdbd',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
}); */