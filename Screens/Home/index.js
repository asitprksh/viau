import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import HomeScreen from './HomePage';

const Home = createStackNavigator
(
    {
        Home : { screen: HomeScreen }
    }
    
);
export default Home;   