/*import React, { Component } from "react";
import { StyleSheet,
         Text,
         View,
        ImageBackground } from 'react-native';


 export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };     
  render() {
    return (
      <View style={styles.container}>
        <Text>Login page</Text>
      </View>
    );
  }    
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#bdbdbd',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
*/

import React from 'react';
import { Container, Button, Text, Header, Content, Form, Item, Input, Toast, Label,Icon } from 'native-base';
import { BackHandler, StyleSheet, Image, View, AsyncStorage, ImageBackground, ActivityIndicator, Animated, screenWidth, Alert } from 'react-native';
import { Stacknavigation } from 'react-navigation';
import Home from "../Home/HomePage";


export default class LoginScreen extends React.Component {

  static navigationOptions = {
    header:null,
    title: 'Login',
  };

  constructor() {
    super();
    this.state = {
      email: '',
      token: '',
      password: '',
      isLoggedIn: 'false',

      data: {
        email: '',
        password: '',
      },
      ActivityIndicator_Loading: false,
    }
  }
  handlePress = async () => {
    this.setState({ ActivityIndicator_Loading: true });
  
  }


  componentDidMount() {
    AsyncStorage.getItem('loggedInStatus',
      (value) => {
        this.setState({ loggedInStatus: value });
      });
  }




  render() {
    const resizeMode = 'contain';
    return (

      <View style={{ flex: 1 }}>
        
        <Container >
          
          
         <Animated.View
            style={{
              width: screenWidth * 0.9 - this.state.titleWidth,
            }}
          />

          <Content >
            <Form >
              <Item floatingLabel >
              <Label>
                Phone No./E-mail
              </Label>
                <Input 
                  
                 // onChangeText={(TextInputText) => this.setState({ email: TextInputText })}*/
               />

              </Item>
              <Item floatingLabel >
              <Label>
                Password
              </Label>
                <Input 
                secureTextEntry={true}
                  
                 
                  //onChangeText={(TextInputText) => this.setState({ password: TextInputText })}
                 
             />
              </Item>
              <Button
                full
                block
                success
                style={{ marginTop: 50,width:300,flex:1,alignSelf:"center" }}
          
                onPress={() =>this.props.navigation.navigate("Home")}  
                
                title="Check Text Input Is Empty or Not"
                color="#2196F3"
              >
              <Text style={{
                            fontWeight:'bold' ,fontSize:15,color:"#FFFFFF"
                          }}>LOGIN       
                </Text>    
              </Button>
              <Button transparent>
              <Text style={{fontSize:12}}>
                Forget Password?
              </Text>

              </Button>
              <Button transparent style={{flex:1,alignSelf:'center'}}>
                <Text style={{fontSize:10}}>

                  New to VIA-U? Register Here!
                </Text>
              </Button>     
            </Form>
          </Content>
        </Container>    
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
form: {
    marginVertical: 15,
    paddingVertical: 150,

  },

  Container: {
    
    flex: 1,   
    paddingVertical: 60,
    // backgroundColor: '#1a237e',
  },
  Content: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  TextInputStyleClass: {

    textAlign: 'center',
    marginBottom: 7,
    height: 40,
    borderWidth: 1
  },
});