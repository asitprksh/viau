import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import LoginScreen from './LoginPage';
import HomeScreen from "../Home/index";

const Login = createStackNavigator
(
    {
        Login : { screen: LoginScreen },
        Home : { screen: HomeScreen }
    }
    
);
export default Login;   