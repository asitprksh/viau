import React from 'react';
import { Container, Button, Text, Header, Content, Form, Item, Input, Toast, Label } from 'native-base';
import { BackHandler, StyleSheet, Image, View, AsyncStorage, ImageBackground, ActivityIndicator, Animated, screenWidth, Alert } from 'react-native';
import { Stacknavigation } from 'react-navigation';
import { CheckBox } from 'react-native-elements';
import Home from "../Home/HomePage";


export default class SignupScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };
  render() {
    const resizeMode = 'contain';
    return (

      <View style={{ flex: 1 }}>
        
        <Container >
          
         
          <Content >
            <Form >
              <Item floatingLabel >
              <Label>
                Full Name
              </Label>
                <Input 
                  
                 // onChangeText={(TextInputText) => this.setState({ email: TextInputText })}*/
               />

              </Item>
              <Item floatingLabel >
              <Label>
                Email
              </Label>
                <Input 
                
                  
                 
                  //onChangeText={(TextInputText) => this.setState({ password: TextInputText })}
                 
             />
              </Item>
              
              <Item floatingLabel >
              <Label>
                Create Password
              </Label>
                <Input secureTextEntry={true}
                
                  
                 
                  //onChangeText={(TextInputText) => this.setState({ password: TextInputText })}
                 
             />
              </Item>
              

              <Item floatingLabel >
              <Label>
                Phone No.
              </Label>
                <Input 
                
                  
                 
                  //onChangeText={(TextInputText) => this.setState({ password: TextInputText })}
                 
             />
              </Item>
              <Item floatingLabel >
              <Label>
               Aadhar No.
              </Label>
                <Input 
                
                  
                 
                  //onChangeText={(TextInputText) => this.setState({ password: TextInputText })}
                 
             />
              </Item>
              <Button
                full
                block
                primary           
                style={{ marginTop: 50,width:300,flex:1,alignSelf:"center" }}
                    
                onPress={() =>this.props.navigation.navigate("Home")}  
                
                title="Check Text Input Is Empty or Not"
                color="#2196F3"
              >
              <Text>SIGNUP</Text>    
              </Button>
              <Button transparent>
              <Text style={{fontSize:12,flex:1,alignSelf:"center"}}>
                Already a member? Login Here
              </Text>

              </Button>
              <Button transparent style={{flex:1,alignSelf:'center'}}>
                <Text style={{fontSize:10}}>

                  By Signing up, You agree to our terms and conditions.
                </Text>
              </Button>     
            </Form>
          </Content>
        </Container>    
      </View>
    );
    
  }
}

const styles = StyleSheet.create({
form: {
    marginVertical: 15,
    paddingVertical: 150,

  },

  Container: {
    
    flex: 1,   
    paddingVertical: 60,
    // backgroundColor: '#1a237e',
  },
  Content: {
    top: 0,
    bottom: 0,
    left: 0,    
    right: 0,
  },
  TextInputStyleClass: {

    textAlign: 'center',
    marginBottom: 7,
    height: 40,
    borderWidth: 1
  },
});