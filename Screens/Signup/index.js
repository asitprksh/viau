import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import SignupScreen from './SignupPage';
import HomeScreen from "../Home/index";

const Signup = createStackNavigator
(
    {
        Signup : { screen: SignupScreen },
        Home : { screen: HomeScreen }
    }
    
);
export default Signup;   